import type { ParsedRecipe } from "$lib/recipes";

let recipe: ParsedRecipe;
let currentSection: string;
let currentInstruction: number;

export function initialiseAssistant(currentRecipe: ParsedRecipe) {
	recipe = currentRecipe;
	currentSection = "description";
	currentInstruction = 0;
}

function outputInformation() {
	switch (currentSection) {
		case "description":
			return recipe.description;
		case "ingredients":
			return "You will need: " + recipe.ingredients.join(", ");
		case "instructions":
			return recipe.instructions[currentInstruction];
		default:
			initialiseAssistant(recipe);
			return "I'm sorry, I have encountered an error. The state has been reset.";
	}
}

function next() {
	switch (currentSection) {
		case "description":
			currentSection = "ingredients";
			break;
		case "ingredients":
			currentSection = "instructions";
			break;
		case "instructions":
			currentInstruction++;
			if (currentInstruction === recipe.instructions.length) {
				currentSection = "description";
				currentInstruction = 0;
			}
			break;
	}
}

function previous() {
	switch (currentSection) {
		case "description":
			currentSection = "instructions";
			currentInstruction = recipe.instructions.length - 1;
			break;
		case "ingredients":
			currentSection = "description";
			break;
		case "instructions":
			currentInstruction--;
			if (currentInstruction < 0) {
				currentSection = "ingredients";
				currentInstruction = 0;
			}
			break;
	}
}

export function processInput(input: string): string | null {
	input = input.toLowerCase();

	if (!input.startsWith("cheap")) {
		return null;
	}

	// Basic hard-coded voice assistant
	if (input.includes("help")) {
		return `You're currently in the ${currentSection} section.
				You can say 'next' to move to the next step, or 'previous' to move to the previous step.
				You can also say 'repeat' to repeat the current section.`;
	}

	if (
		input.includes("next") ||
		input.includes("continue") ||
		input.includes("forward")
	) {
		next();
		return outputInformation();
	}

	if (input.includes("previous") || input.includes("back")) {
		previous();
		return outputInformation();
	}

	if (input.includes("repeat")) {
		return outputInformation();
	}

	return "I'm sorry, I didn't understand that.";
}
