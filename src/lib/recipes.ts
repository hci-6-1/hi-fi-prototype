import rawRecipes from "./recipes.json";

export type RawRecipe = {
	recipe_title: string;
	url: string;
	record_health: string;
	vote_count: string;
	rating: string;
	description: string;
	cuisine: string;
	course: string;
	diet: string;
	prep_time: string;
	cook_time: string;
	ingredients: string;
	instructions: string;
	author: string;
	tags: string;
	category: string;
	id: string;
};

export type ParsedRecipe = {
	id: number;
	title: string;
	url: string;
	votes: number;
	rating: number;
	description: string;
	// For now
	cuisine: string;
	course: string;
	diet: string;
	tags: string[];
	preparationTime: number;
	cookingTime: number;
	ingredients: string[];
	instructions: string[];
};

export function parseRecipe(rawRecipe: RawRecipe): ParsedRecipe | null {
	try {
		const parsedRecipe = {
			id: parseInt(rawRecipe.id),
			title: rawRecipe.recipe_title.trim(),
			url: rawRecipe.url.trim(),
			votes: parseInt(rawRecipe.vote_count),
			rating: parseFloat(rawRecipe.rating),
			description: rawRecipe.description.trim(),
			cuisine: rawRecipe.cuisine,
			course: rawRecipe.course,
			diet: rawRecipe.diet,
			tags: rawRecipe.tags.split("|"),
			preparationTime: parseInt(rawRecipe.prep_time.slice(0, -2)),
			cookingTime: parseInt(rawRecipe.cook_time.slice(0, -2)),
			ingredients: rawRecipe.ingredients.split("|"),
			instructions: rawRecipe.instructions.split("|"),
		} as ParsedRecipe;

		if (parsedRecipe.title === "") {
			return null;
		}
		if (parsedRecipe.url === "") {
			return null;
		}
		if (isNaN(parsedRecipe.votes)) {
			return null;
		}
		if (isNaN(parsedRecipe.rating)) {
			return null;
		}
		if (parsedRecipe.description === "") {
			return null;
		}
		if (
			parsedRecipe.cuisine === "" ||
			parsedRecipe.cuisine.startsWith(" ")
		) {
			return null;
		}
		parsedRecipe.cuisine = parsedRecipe.cuisine.trim();
		if (parsedRecipe.course === "" || parsedRecipe.course.startsWith(" ")) {
			return null;
		}
		parsedRecipe.course = parsedRecipe.course.trim();
		if (parsedRecipe.diet === "" || parsedRecipe.diet.startsWith(" ")) {
			return null;
		}
		parsedRecipe.diet = parsedRecipe.diet.trim();
		for (const tag of parsedRecipe.tags) {
			if (tag === "" || tag.startsWith(" ")) {
				return null;
			}
		}
		parsedRecipe.tags = parsedRecipe.tags.map((tag) => tag.trim());
		if (isNaN(parsedRecipe.preparationTime)) {
			return null;
		}
		if (isNaN(parsedRecipe.cookingTime)) {
			return null;
		}
		for (const ingredient of parsedRecipe.ingredients) {
			if (ingredient === "" || ingredient.startsWith(" ")) {
				return null;
			}
		}
		parsedRecipe.ingredients = parsedRecipe.ingredients.map((ingredient) =>
			ingredient.trim()
		);
		for (const instruction of parsedRecipe.instructions) {
			if (instruction === "" || instruction.startsWith(" ")) {
				return null;
			}
		}
		parsedRecipe.instructions = parsedRecipe.instructions.map(
			(instruction) => instruction.trim()
		);

		return parsedRecipe;
	} catch (error) {
		// We have enough recipes, so we can ignore the ones that are broken
		return null;
	}
}

const parsedRecipes = (Object.values(rawRecipes) as RawRecipe[])
	.map(parseRecipe)
	.filter((recipe) => recipe !== null) as ParsedRecipe[];

const cuisineSet: Set<string> = new Set();
const courseSet: Set<string> = new Set();
const dietSet: Set<string> = new Set();
const tagSet: Set<string> = new Set();

parsedRecipes.forEach((recipe) => {
	cuisineSet.add(recipe.cuisine);
	courseSet.add(recipe.course);
	dietSet.add(recipe.diet);
	recipe.tags.forEach((tag) => tagSet.add(tag));
});

export const cuisines: string[] = Array.from(cuisineSet).sort();
export const courses: string[] = Array.from(courseSet).sort();
export const diets: string[] = Array.from(dietSet).sort();
export const tags: string[] = Array.from(tagSet).sort();

export function logStatistics() {
	console.log(
		parsedRecipes.filter((recipe) => ["Eggetarian"].includes(recipe.diet))
	);
}

export function getRecipe(id: number): ParsedRecipe | undefined {
	return parsedRecipes.find((recipe) => recipe.id === id);
}

export function filterRecipes(
	cuisines: string[],
	courses: string[],
	diets: string[],
	tags: string[],
	maxPreparationTime: number,
	maxCookingTime: number
): ParsedRecipe[] {
	return parsedRecipes.filter((recipe) => {
		if (cuisines.length > 0 && !cuisines.includes(recipe.cuisine)) {
			return false;
		}
		if (courses.length > 0 && !courses.includes(recipe.course)) {
			return false;
		}
		if (diets.length > 0 && !diets.includes(recipe.diet)) {
			return false;
		}
		if (
			tags.length > 0 &&
			!tags.every((tag) => recipe.tags.includes(tag))
		) {
			return false;
		}
		if (
			maxPreparationTime > 0 &&
			recipe.preparationTime > maxPreparationTime
		) {
			return false;
		}
		if (maxCookingTime > 0 && recipe.cookingTime > maxCookingTime) {
			return false;
		}

		return true;
	});
}
