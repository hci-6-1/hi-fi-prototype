import { writable } from "svelte/store";

export const preferredCuisines = writable<string[]>([]);
export const preferredCourses = writable<string[]>([]);
export const preferredDiets = writable<string[]>([]);
export const preferredTags = writable<string[]>([]);
export const preferredMaxPreparationTime = writable<number>(0);
export const preferredMaxCookingTime = writable<number>(0);
