import { error } from "@sveltejs/kit";
import { to_number } from "svelte/internal";
import { getRecipe } from "$lib/recipes";

type Params = {
	params: {
		id: string;
	};
};

export function load({ params }: Params) {
	const id = to_number(params.id);
	if (isNaN(id)) {
		throw error(400, "Non-numeric id");
	}

	const recipe = getRecipe(id);
	if (!recipe) {
		throw error(404, "Recipe not found");
	}

	return {
		recipe,
	};
}
